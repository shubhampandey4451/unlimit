import 'dart:async';

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:unlimit/controller/my_home_controller.dart';

class MyHomePage extends StatefulWidget {
  const MyHomePage({super.key, required this.title});
  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  int _counter = 0;
  final myHomeController = Get.put(MyHomeController());

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    myHomeController.fetchingJokes();
    timerClock();
  }

  timerClock(){
    Timer.periodic(const Duration(minutes: 1), (timer) {
      if (kDebugMode) {
        print("Time Start $timer");
      }
      myHomeController.fetchingJokes();
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: RefreshIndicator(
        onRefresh: () async{
          myHomeController.fetchingJokes();
        },
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisSize: MainAxisSize.min,
          children: [
            Expanded(child: Obx(() {
              if (myHomeController.isLoading.value) {
                return const Center(child: CircularProgressIndicator());
              } else {
                return Padding(padding: EdgeInsets.all(10),
                    child:ListView(
                        children:
                        List.generate(myHomeController.jokes.length, (index) {
                          return Center(
                              child: Card(
                                  child:  Padding(
                                    padding: const EdgeInsets.all(10),
                                    child: Column(
                                      crossAxisAlignment: CrossAxisAlignment.start,
                                      children: [
                                        Text("Jokes ${index +1 }",
                                          style: const TextStyle(color: Colors.grey),),
                                        const SizedBox(height: 5,),
                                        Text(myHomeController.jokes[index].joke??"",
                                          style: const TextStyle(color: Colors.black,fontSize: 20),)
                                      ],
                                    ),
                                  )
                              ));
                        }))
                );
              }
            }))
          ],
        ),
      ), // This trailing comma makes auto-formatting nicer for build methods.
    );
  }
}