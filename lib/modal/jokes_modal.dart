class JokesModal {
  late String joke;

  JokesModal({required this.joke});

  JokesModal.fromJson(Map<String, dynamic> json) {
    joke = json['joke'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['joke'] = joke;
    return data;
  }
}