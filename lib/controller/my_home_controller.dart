

import 'package:get/state_manager.dart';
import 'package:unlimit/network/apis.dart';



class MyHomeController extends GetxController {
  var isLoading = true.obs;
  var jokes = [].obs;

  void fetchingJokes() async {
    try {
      isLoading(true);
      var res = await APIHandler.fetchJokes();
      if(jokes.length <= 9){
        jokes.add(res);
      }else{
        jokes.removeAt(0);
        jokes.add(res);
      }

    } finally {
      isLoading(false);
    }
  }
}