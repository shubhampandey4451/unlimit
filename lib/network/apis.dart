import 'dart:convert';

import 'package:unlimit/modal/jokes_modal.dart';
import 'package:http/http.dart' as http;

class APIHandler{

  static Future fetchJokes() async {
    var response = await http
        .get(Uri.parse('https://geek-jokes.sameerkumar.website/api?format=json'));
    if (response.statusCode == 200) {
      var jsonData = response.body;
      var jokes = JokesModal.fromJson(json.decode(jsonData));
      return jokes;
    } else {
      return null;
    }
  }
}